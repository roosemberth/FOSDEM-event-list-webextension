function setup() {
  console.log("Foo")
  function extractPageInfo() {
    let panelElements = Array.from(document.querySelectorAll('#main .side-box > li'))

    function findTagged(tag) {
      let target = panelElements.filter(e => e.querySelector('strong').innerHTML == tag)[0]
      if (!target)
        return
      return target.querySelector('a').innerHTML
    }

    let day = ({
      Saturday: '2022-02-05',
      Sunday: '2022-02-06',
    })[findTagged("Day")]

    if (!day)
      return

    let from = new Date(`${day}T` + findTagged("Start"))
    let to = new Date(`${day}T` + findTagged("End"))

    if (!from || !to)
      return

    let title = document.querySelector('#main > .breadcrumbs li:last-child').innerHTML
    let url = document.URL

    return { title, from, to, url }
  }

  const eventDetails = extractPageInfo()

  browser.runtime.sendMessage("fosdem-collector@orbstheorem.ch", {
    request: "fc.updatePageIcon",
    value: eventDetails,
  }).catch(e => console.log(`Could not update page action icon: ${e}`))

  let handlers = {
    "fcp.addEvent": (_, __, sendResponse) => {
      if (!eventDetails) {
        console.error("Could not parse any events in the current page.")
        return
      }
      browser.runtime.sendMessage("fosdem-collector@orbstheorem.ch", {
        request: "fc.addEvent",
        value: eventDetails,
      }).then(sendResponse)
    },
    "fcp.removeEvent": (_, __, sendResponse) => {
      browser.runtime.sendMessage("fosdem-collector@orbstheorem.ch", {
        request: "fc.removeEvent",
        value: document.URL,
      }).then(sendResponse)
    },
  }

  browser.runtime.onMessage.addListener((msg, sender, sendResponse) => {
    handlers[msg.command]?.(msg, sender, sendResponse)
  })
}

// Javascript _usually_ runs before the page has finished loading, but in the
// case the page was cached (such when user does an "Undo close tab"), by the
// time our code runs the onload event will have already been fired.
window.onload = setup()
if (document.readyState !== "loading") {
  setup()
}
