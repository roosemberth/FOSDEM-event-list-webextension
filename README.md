# FOSDEM events firefox extension

The objective is pretty simple: Add a [_page action_][] to track fosdem
events and then show them in a simple list.

(Also for me to learn about webextensions.)

[_page action_]: https://developer.chrome.com/docs/extensions/reference/pageAction/

![Click to add the event in this page](img/page-action-closed.png)

![The event in this page was added!](img/open-page-action.png)

## Installing

[**Download from the CI**][ci-ffox] and install. Use at your own risk ⚠️.

[ci-ffox]: https://gitlab.com/roosemberth/FOSDEM-event-list-webextension/-/jobs/artifacts/master/raw/fosdemcollect-latest.xpi?job=Build%20firefox%20extension

I don't know whether I want to publish this yet (probably not).
The way I install it is by navigating to <about:debugging#/runtime/this-firefox>,
clicking on _Load temporary add-on_ and then selecting the `manifest.json` on
this repository.

## Notes

The way we scrap the data is ugly, but so is the web...

The list generated at the moment is pretty simple: Day, start time, stop time
and link.
