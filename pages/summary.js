window.onload = function() {
  let clearListBtn = document.getElementById('clearListBtn')
  let emptyListMsg = document.getElementById('emptyListMsg')

  browser.storage.local.get("events").then(stored => {
    let events = stored.events.sort((a, b) => a.from - b.from)

    if (events.length == 0) {
      emptyListMsg.classList.remove("hidden")
    } else {
      clearListBtn.classList.remove("hidden")
    }

    function renderEvent(ev) {
      let dateStrOpts = { weekday: 'short', hour: '2-digit', minute: '2-digit' }
      let from = ev.from.toLocaleDateString(undefined, dateStrOpts)
      let to = ev.to.toLocaleDateString(undefined, {...dateStrOpts, weekday: 'narrow' })
      return `<li class="event">${from}-${to}: <a href=${ev.url}>${ev.title}</a></li>`
    }

    document.getElementById('events-list').innerHTML =
      [...events].reduce((t, ev) => t + renderEvent(ev), "")
    document.getElementById('progress-indicator').innerHTML = ""
  })

  clearListBtn.addEventListener("click", e => {
    e.target.disabled = true
    browser.runtime.sendMessage("fosdem-collector@orbstheorem.ch", {
      request: "fc.clearEventData",
    }).then(() => location.reload())
      .catch(e => console.log(`Could not clear event data: ${e}`))
  })
}
