window.onload = function() {
  let onUserCommand = {
    "removeEvent": t => browser.tabs.sendMessage(t.id, {
      command: "fcp.removeEvent",
    }).then(window.close),
    "close": () => window.close(),
    "openList": () => browser.tabs.create({"url": "/pages/summary.html"}),
  }

  // Calls the specified action with the the active tab or undefined.
  let withTab = cb => browser.tabs.query({active: true, currentWindow: true})
      .then(tabs => cb(tabs.find(() => true /* Any will do. */)))

  document.addEventListener("click", e => {
    if (e.target.classList.contains("fcCommand")) {
      let cmd = e.target?.getAttribute("cmd")
      console.log(`Page action: Handling command ${cmd}`)
      withTab(t => onUserCommand[cmd]?.(t))
        .catch(e => console.error(`Error handling command: ${e}`))
    }
  })

  withTab(t => browser.tabs.sendMessage(t.id, {
    command: "fcp.addEvent",
  })).catch(e => console.error(`Error adding event: ${e}`))
}
